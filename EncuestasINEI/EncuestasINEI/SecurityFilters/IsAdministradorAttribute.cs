﻿using EncuestasINEI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EncuestasINEI.SecurityFilters
{
    public class IsAdministradorAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool result = base.AuthorizeCore(httpContext);
            if (httpContext.Session["CurrentUser"] != null)
            {
                Usuario objUsuario = (Usuario)httpContext.Session["CurrentUser"];
                if (objUsuario.TipoUsuario.Nombre.ToLower() == "administrador")
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }else
            {
                result = false;
            }
            return result;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Controller.TempData["ErrorMessage"] = "¡Ud. no puede acceder a esta opción!";
            filterContext.Result = new RedirectToRouteResult(
                                   new System.Web.Routing.RouteValueDictionary
                                   {
                                       {"action", "Login" },
                                       {"controller", "Usuarios" }
                                   });
        }
    }
}