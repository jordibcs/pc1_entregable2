﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EncuestasINEI.Startup))]
namespace EncuestasINEI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
