﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EncuestasINEI.Models;
using EncuestasINEI.Controllers;
using System.Data.SqlClient;

namespace EncuestasINEI.Services
{
    public class TipoPregutaService : ITipoPreguntaService
    {
        private bool state;
        TipoPreguntasController tipoPreguntasController;
        public TipoPregutaService()
        {
            tipoPreguntasController = new TipoPreguntasController();
        }
        public bool Create(TipoPregunta obj)
        {
            try
            {
                tipoPreguntasController.Create(obj);
                state = true;
            } catch(SqlException e)
            {
                e.Errors.ToString();
            }
            return state;
        }

        public bool Delete(int id)
        {
            try
            {
                tipoPreguntasController.DeleteConfirmed(id);
                state = true;
            } catch(SqlException e)
            {
                e.Errors.ToString();
            }
            return state;
        }

        public bool Details(int id)
        {
            try
            {
                tipoPreguntasController.Details(id);
                state = true;
            }catch(SqlException e)
            {
                e.Errors.ToString();
            }
            return state;
        }

        public bool Edit(int id)
        {
            try
            {
                tipoPreguntasController.Edit(id);
                state = true;
            }
            catch (SqlException e)
            {
                e.Errors.ToString();
            }
            return state;
        }
    }
}