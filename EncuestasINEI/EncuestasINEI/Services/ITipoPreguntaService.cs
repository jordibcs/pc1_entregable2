﻿using EncuestasINEI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncuestasINEI.Services
{
    public interface ITipoPreguntaService
    {
        bool Details(int id);
        bool Create(TipoPregunta obj);
        bool Edit(int id);
        bool Delete(int id);
    }
}
