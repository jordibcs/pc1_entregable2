//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EncuestasINEI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Encuesta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Encuesta()
        {
            this.Pregunta = new HashSet<Pregunta>();
            this.Usuario_Encuesta = new HashSet<Usuario_Encuesta>();
        }

        
        
        public int ID { get; set; }
        [Required(ErrorMessage ="Este campo es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public System.DateTime Fecha { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        [Range(0.01,999, ErrorMessage ="El costo debe ser entre 0 y 999.")]
        public decimal Costo { get; set; }
        [Display(Name = "�Es de alcance nacional?")]
        public bool AlcanceNacional { get; set; }
        [Display(Name = "�Est� activo?")]
        public bool Estado { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pregunta> Pregunta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario_Encuesta> Usuario_Encuesta { get; set; }
    }
}
