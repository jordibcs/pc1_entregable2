﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EncuestasINEI.Models;
using EncuestasINEI.Controllers;
using Moq;
using System.Web.Mvc;
using EncuestasINEI.Services;

namespace UnitTestProjectEncuestasINEI
{
    [TestClass]
    public class TipoPreguntasControllerTest
    {
        [TestMethod]
        public void Details()
        {
            var mock = new Mock<ITipoPreguntaService>();
            bool validResult, invalidResult;
            TipoPreguntasController tipoPreguntaController = new TipoPreguntasController();
            mock.Setup(m => m.Details(It.Is<int>(n => n.Equals(1)))).Returns(true);
            mock.Setup(m => m.Details(It.Is<int>(n => n.Equals(-1)))).Returns(false);
            validResult = mock.Object.Details(1);
            invalidResult = mock.Object.Details(-1);
            Assert.IsTrue(validResult);
            Assert.IsFalse(invalidResult);
        }

        [TestMethod]
        public void Create()
        {
            var mock = new Mock<ITipoPreguntaService>();
            bool validResult, invalidResult;
            TipoPreguntasController tipoPreguntaController = new TipoPreguntasController();
            mock.Setup(m => m.Create(It.IsAny<TipoPregunta>())).Returns(true);
            mock.Setup(m => m.Create(null)).Returns(false);
            validResult = mock.Object.Create(new TipoPregunta());
            invalidResult = mock.Object.Create(null);
            Assert.IsTrue(validResult);
            Assert.IsFalse(invalidResult);
        }

        [TestMethod]
        public void Delete()
        {
            var mock = new Mock<ITipoPreguntaService>();
            bool validResult, invalidResult;
            TipoPreguntasController tipoPreguntaController = new TipoPreguntasController();
            mock.Setup(m => m.Delete(It.Is<int>(n => n.Equals(1)))).Returns(true);
            mock.Setup(m => m.Delete(It.Is<int>(n => n.Equals(-1)))).Returns(false);
            validResult = mock.Object.Delete(1);
            invalidResult = mock.Object.Delete(-1);
            Assert.IsTrue(validResult);
            Assert.IsFalse(invalidResult);
        }

        [TestMethod]
        public void Edit()
        {
            var mock = new Mock<ITipoPreguntaService>();
            bool validResult, invalidResult;
            TipoPreguntasController tipoPreguntaController = new TipoPreguntasController();
            mock.Setup(m => m.Edit(It.IsAny<int>())).Returns(true);
            mock.Setup(m => m.Edit(It.Is<int>(n => n.Equals(-1)))).Returns(false);
            validResult = mock.Object.Edit(1);
            invalidResult = mock.Object.Edit(-1);
            Assert.IsTrue(validResult);
            Assert.IsFalse(invalidResult);
        }
    }
}
